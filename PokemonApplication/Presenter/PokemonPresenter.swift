//
//  PokemonPresenter.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/26/21.
//

import Foundation
import UIKit

protocol PokemonPresenterDelegate:AnyObject{
func presentPokemons(pokemons:[Pokemon])
    

}

typealias PresenterDelegate = PokemonPresenterDelegate & UIViewController
class PokemonPresenter{
    
    
    
    weak var delegate: PresenterDelegate?
     static var curerntPokemonPresenting:Pokemon?
    
    public func setViewdelegate(delegate:PokemonPresenterDelegate & UIViewController){
        self.delegate = delegate
    }
    
    public func getPokemons(){
        self.delegate?.presentPokemons(pokemons: DataProcessing.pokemons)
    }
    

    
    public func incrementPage(){
        
        DataProcessing.offset += 10
      
        DataProcessing.getData()
        
    }
    
    public func decrementPage(){
        
        DataProcessing.offset -= 10
      
        DataProcessing.getData()
      
    }
    
    func disableButton(button : UIButton){
        
        if DataProcessing.offset == 0{
            button.isEnabled = false}
    else{
    button.isEnabled = true
    }
    
    }
}
