//
//  PokemonDetailspresenter.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/28/21.
//

import Foundation
import UIKit
protocol PokemonDetailsPresenterDelegate:AnyObject{
func presentPokemonDetails(pokemon:Pokemon)
    
//    func setForPresentingPokemonDetails(pokemon:Pokemon)
}
typealias PresenterDetailsDelegate = PokemonDetailsPresenterDelegate & UIViewController
class PokemosDetailspresenter{
    
    weak var delegate: PresenterDetailsDelegate?
    static var curerntPokemonPresenting:Pokemon?
    
    
    public func setCurerntPokemonPresenting(pokemon:Pokemon){
        PokemonPresenter.curerntPokemonPresenting = pokemon
    }
    
    public func getPokemons(){
        self.delegate?.presentPokemonDetails(pokemon: PokemosDetailspresenter.curerntPokemonPresenting!)
    }
    
    
}
