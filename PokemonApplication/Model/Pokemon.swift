//
//  Pokemon.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/26/21.
//

import Foundation



import Foundation


struct Pokemon: Codable {
    let id: Int
    let name: String
    let sprites: Sprites
    let stats: [Stat]
    let types: [TypeElement]

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case  sprites, stats, types
    }
}


struct Species: Codable {
    let name: String
    let url: String
}


struct TypeElement: Codable {
    let slot: Int
    let type: Species
}


struct Sprites: Codable {
    let backDefault: String
    let backFemale: String?
    let backShiny: String
    let backShinyFemale: String?
    let frontDefault: String
    let frontFemale: String?
    let frontShiny: String
    let frontShinyFemale: String?
 
    enum CodingKeys: String, CodingKey {
        case backDefault = "back_default"
        case backFemale = "back_female"
        case backShiny = "back_shiny"
        case backShinyFemale = "back_shiny_female"
        case frontDefault = "front_default"
        case frontFemale = "front_female"
        case frontShiny = "front_shiny"
        case frontShinyFemale = "front_shiny_female"
    }
    
 

}


struct Stat: Codable {
    let baseStat, effort: Int
    let stat: Species

    enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat"
        case effort, stat
    }
}




