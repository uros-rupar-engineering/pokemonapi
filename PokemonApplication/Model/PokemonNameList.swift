//
//  PokemonNameList.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/26/21.
//

import Foundation

struct PokemonList: Codable {
    let count: Int
    let next:String?
    let previous: String?
    let results: [Result]
}

// MARK: - Result
struct Result: Codable {
    let name: String
    let url: String
}
