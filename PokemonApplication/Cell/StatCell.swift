//
//  StatCell.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/27/21.
//

import UIKit

class StatCell: UITableViewCell {
    @IBOutlet weak var statName: UILabel!
    @IBOutlet weak var statValue: UILabel!
    @IBOutlet weak var statSlider: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
