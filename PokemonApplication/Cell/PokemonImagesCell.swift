//
//  PokemonImagesCell.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/27/21.
//

import UIKit

class PokemonImagesCell: UICollectionViewCell {
    
    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var pokemonName: UILabel!
}
