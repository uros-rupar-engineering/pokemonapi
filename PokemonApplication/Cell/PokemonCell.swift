//
//  PokemonCell.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/26/21.
//

import UIKit

class PokemonCell: UITableViewCell {

    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var pokemonNamlabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        innerView.layer.cornerRadius = 16
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
