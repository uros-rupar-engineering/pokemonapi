//
//  DataProcessing.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/26/21.
//

import Foundation

struct DataProcessing{
    static let jsonDecoder = JSONDecoder()
    static var pokemonList: [Result]?
     static var offset = 0;
    
    static var limit = 1;
    
    
    static var urlString = "https://pokeapi.co/api/v2/pokemon?limit=\(limit)&offset=\(offset)"
    static let url = URL(string: urlString)!
    static var task: URLSessionTask!
    
    static var fullPokemonInfo: PokemonList?
    
    static var pokemons:[Pokemon] = []
    static var offlinePokemons:[String] = []
    
    static func getData(){
   
       
        task = URLSession.shared.dataTask(with: returnUrl(offset: offset)){[self] (data, response, error) in
            
            if let data = data, let fullPokemonInfo1: PokemonList = try? JSONDecoder().decode(PokemonList.self, from: data){
        
                print(offset)
                        fullPokemonInfo = fullPokemonInfo1

                        pokemonList = fullPokemonInfo1.results

                
                if let sizeOfListe = fullPokemonInfo?.results.count{
                    
                   
                    
                    if(pokemons.count > 0){
                    pokemons = []
                        
                    }
                    
                    
                    for i in 0...sizeOfListe - 1{
                       
                        let url:String = (fullPokemonInfo?.results[i].url)!
                        
                        DispatchQueue.main.async {
                            URLSession.shared.dataTask(with: URL(string:url)!){ (data, response, error) in
                                 if let data = data, let pokemon: Pokemon = try? JSONDecoder().decode(Pokemon.self, from: data){
                                   
                                     self.pokemons.append(pokemon)
                                    offlinePokemons.append(pokemon.name)
                                    
                                 }
                            }.resume()
                        }
                      
                    }


                }
                          
                
            }
            
        }
        
        task.resume()

        UserDefaults.standard.setValue(offlinePokemons, forKey: "offlineDatas")
       
        
        offlinePokemons = UserDefaults.standard.object(forKey: "offlineDatas") as? [String] ?? []
  
        
    }
    


    static func returnUrl(offset:Int)->URL{
        
        if self.offset < 0{
            self.offset = 0
        }
        return URL(string: "https://pokeapi.co/api/v2/pokemon?limit=10&offset=\(offset)")!
    }

}
