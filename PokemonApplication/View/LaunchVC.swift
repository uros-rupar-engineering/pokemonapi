//
//  LaunchVC.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/26/21.
//

import UIKit

@available(iOS 13.0, *)
class LaunchVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork(){
            DataProcessing.getData()
        }else{
            print("Internet Connection not Available!")
        }
        
      
        self.navigationController?.isNavigationBarHidden = true
        

    }

    
    @IBAction func launchData(_ sender: Any) {
       performSegue(withIdentifier: "HomeVC", sender: self)
        
    }
    
 

}
