//
//  PokemonDetailsVC.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/27/21.
//

import UIKit

class PokemonDetailsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,PokemonDetailsPresenterDelegate {
    
    
  
    
 
    
    
    @IBOutlet weak var type2Label: UILabel!
    
    @IBOutlet weak var sliderImages: UICollectionView!
    @IBOutlet weak var pokemonName: UILabel!
    @IBOutlet weak var pokemonType: UILabel!
    
    private let presenter = PokemosDetailspresenter()
    
    var presentingPokemon:Pokemon!
    var images:[String]?
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUI()
        title = PokemonPresenter.curerntPokemonPresenting?.name
        fillArrayOfImages()
        
      
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func fillArrayOfImages() {
        images = [
            PokemonPresenter.curerntPokemonPresenting!.sprites.frontDefault,
            PokemonPresenter.curerntPokemonPresenting!.sprites.backDefault,
            PokemonPresenter.curerntPokemonPresenting!.sprites.frontShiny,
            PokemonPresenter.curerntPokemonPresenting!.sprites.backShiny,
            PokemonPresenter.curerntPokemonPresenting!.sprites.frontFemale ?? "https://www.freeiconspng.com/thumbs/x-png/x-png-33.png",
            PokemonPresenter.curerntPokemonPresenting!.sprites.backFemale ?? "https://www.freeiconspng.com/thumbs/x-png/x-png-33.png",
            PokemonPresenter.curerntPokemonPresenting!.sprites.frontShinyFemale ??  "https://www.freeiconspng.com/thumbs/x-png/x-png-33.png",
            PokemonPresenter.curerntPokemonPresenting!.sprites.backShinyFemale ?? "https://www.freeiconspng.com/thumbs/x-png/x-png-33.png"
        ]
       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! PokemonImagesCell
        
        if let urlString = images?[indexPath.row],let url = URL(string: urlString){
            URLSession.shared.dataTask(with: url){(data,response,error) in
                if let data = data{
                  
                    DispatchQueue.main.async {
                        cell.pokemonImage.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.width
        let height = sliderImages.frame.height

        return CGSize(width: width, height: height)
    }
    
   func initializeUI(){
    pokemonName.text = PokemonPresenter.curerntPokemonPresenting?.name
    
    if PokemonPresenter.curerntPokemonPresenting?.types.count == 2{
    pokemonType.text = PokemonPresenter.curerntPokemonPresenting?.types[0].type.name
   
        type2Label.text = PokemonPresenter.curerntPokemonPresenting?.types[1].type.name}
    else if PokemonPresenter.curerntPokemonPresenting?.types.count == 1{
        pokemonType.text = PokemonPresenter.curerntPokemonPresenting?.types[0].type.name
    }
   


   }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return PokemonPresenter.curerntPokemonPresenting?.stats.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "statCell", for: indexPath) as! StatCell
        
        cell.statName.text = PokemonPresenter.curerntPokemonPresenting!.stats[indexPath.row].stat.name
        cell.statValue.text = "\(PokemonPresenter.curerntPokemonPresenting!.stats[indexPath.row].baseStat)"
        
        cell.statSlider.value = Float(PokemonPresenter.curerntPokemonPresenting?.stats[indexPath.row].baseStat ?? 0)
        
        return cell
    }
    
    func presentPokemonDetails(pokemon: Pokemon) {
        presentingPokemon = pokemon
    }
}
