//
//  ViewController.swift
//  PokemonApplication
//
//  Created by uros.rupar on 7/26/21.
//

import UIKit

@available(iOS 11.0, *)
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PokemonPresenterDelegate {
 

    @IBOutlet weak var internetConectionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var pagrNumber: UILabel!
    
    @IBOutlet weak var pokemonTable: UITableView!
    
    private let presenter = PokemonPresenter()
    private let presenterDetails = PokemosDetailspresenter()
    
    private var pokemons = [Pokemon?]()
    private var offlinePokemons:[String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        print("-----------------------")
        print(DataProcessing.offlinePokemons)
        presenter.setViewdelegate(delegate: self)
        presenter.getPokemons()
       
       
       
        loadUI()
        
        if !Reachability.isConnectedToNetwork(){
//            presenter.disableButton(button: backButton)
//            presenter.disableButton(button: nextButton)
            internetConectionLabel.isHidden = false
            internetConectionLabel.layer.zPosition = 1
        }else{
            internetConectionLabel.isHidden = true
        }
  
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pokemons.count    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell", for: indexPath) as! PokemonCell
        
        
        if Reachability.isConnectedToNetwork(){
            cell.pokemonNamlabel.text = pokemons[indexPath.row]?.name
            if let urlString = pokemons[indexPath.row]?.sprites.frontDefault,let url = URL(string: urlString){
                URLSession.shared.dataTask(with: url){(data,response,error) in
                    if let data = data{
                      
                        DispatchQueue.main.async {
                            cell.pokemonImage.image = UIImage(data: data)
                        }
                    }
                }.resume()
            }
        }else{
            
            
            cell.pokemonNamlabel.text = offlinePokemons[indexPath.row]
//          cell.pokemonImage.image = UIImage(named: "")
            
        }
        
        
        
        tableView.rowHeight = 300
        
        
      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        presenterDetails.setCurerntPokemonPresenting(pokemon: pokemons[indexPath.row]!)
        
        performSegue(withIdentifier: "pockemonDetails", sender: self)
    }


    
    @IBAction func nextPage(_ sender: Any) {
       
        presenter.incrementPage()
        presenter.disableButton(button: backButton)
        pagrNumber.text = String((DataProcessing.offset+10)/10)
        presenter.getPokemons()
      
        
        DispatchQueue.main.async {
            self.pokemonTable.reloadData()
        }
    }
    
    @IBAction func previousPage(_ sender: Any) {
        
        
        
        presenter.decrementPage()
        presenter.disableButton(button: backButton)
        presenter.getPokemons()
        pagrNumber.text = String((DataProcessing.offset+10)/10)
        DispatchQueue.main.async {
            self.pokemonTable.reloadData()
        }
        
       
    }
    
    func presentPokemons(pokemons: [Pokemon]) {
        self.pokemons = DataProcessing.pokemons.count != 0 ? pokemons : pokemons
        
        self.offlinePokemons = DataProcessing.offlinePokemons
        DispatchQueue.main.async {
            self.pokemonTable.reloadData()
        }
    }

    func loadUI(){
        pagrNumber.text = String((DataProcessing.offset+10)/10)
    }
}


